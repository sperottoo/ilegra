//
//  ProvaTecnicaUITests.swift
//  ProvaTecnicaUITests
//
//  Created by Fabrício Sperotto Sffair on 2018-01-09.
//  Copyright © 2018 Ilegra. All rights reserved.
//
//  Testes de UI do app

import XCTest

class ProvaTecnicaUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = true
        
        XCUIApplication().launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    /// Teste básico de UI da tableview e de clique/navegação
    func testClickAndTableView() {
        
        let app = XCUIApplication()
        let element = app.tables.staticTexts["Abomination (Emil Blonsky)"]
        
        self.waitFor(element: element) {
            $0.exists
        }
        
        XCUIApplication().tables/*@START_MENU_TOKEN@*/.staticTexts["Abomination (Emil Blonsky)"]/*[[".cells.staticTexts[\"Abomination (Emil Blonsky)\"]",".staticTexts[\"Abomination (Emil Blonsky)\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        let backButton = app.navigationBars["Abomination (Emil Blonsky)"].buttons["Back"]
        backButton.tap()
        
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Aaron Stack"]/*[[".cells.staticTexts[\"Aaron Stack\"]",".staticTexts[\"Aaron Stack\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()

        
    }
    
}


extension XCTestCase {
    
    
    /// Função para esperar um elemento específicio aparecer
    ///
    /// - Parameters:
    ///   - element: elemento a ser esperado para aparecer
    ///   - timeout: TimeInterval esperado para timeout(default 10)
    ///   - expectationPredicate: predicado para quando o elemento aparecer
    func waitFor(element: XCUIElement, timeout: TimeInterval = 10, expectationPredicate: @escaping (XCUIElement) -> Bool) {
        let predicate = NSPredicate { elem, _ in
            expectationPredicate(elem as! XCUIElement)
        }
        expectation(for: predicate, evaluatedWith: element, handler: nil)
        
        waitForExpectations(timeout: timeout) { error in
            if (error != nil) {
                let message = "Erro ao esperar \(element) depois de \(timeout) segundos."
            }
        }
    }
    
}
