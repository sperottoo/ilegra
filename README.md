# O Projeto #

Este projeto tem como finalidade listar todos os personagens da Marvel, utilizando a API disponível em http://developer.marvel.com, além de mostrar algumas informações, como a descrição do personagem, numero de comics, entre outros.

### Como rodar? ###

Para rodar este app, não é necessário nenhum set up adicional. Foi utilizado Carthage para criar dependencias da lib Kingfisher, lib que facilita o download e exibição de imagens.
Logo, só será necessário clonar, abrir o xcode e rodar no simulador ou iOS.
