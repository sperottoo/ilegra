//
//  APITests.swift
//  ProvaTecnicaTests
//
//  Created by Fabrício Sperotto Sffair on 2018-01-09.
//  Copyright © 2018 Fabrício Sperotto Sffair. All rights reserved.
//
//  Testes de requisição.

import XCTest

@testable import ProvaTecnica

class APITests: XCTestCase {

    func testGetRequest() {
        guard let urlToTest = URL(string: "https://google.com") else {
            XCTFail("ERRO: URL INVÁLIDA")
            return
        }
        
        let promise = expectation(description: "Status code: 200")
        
        API.createRequest(url: urlToTest) { (data, response, error) in
            if let erro = error {
                XCTFail("ERRO: \(erro.localizedDescription)")
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200 {
                   promise.fulfill()
                } else {
                    XCTFail("ERRO STATUSCODE: \(statusCode)")
                }
            } else {
                XCTFail("ERRO SEM STATUS CODE?")
            }
        }
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
}
