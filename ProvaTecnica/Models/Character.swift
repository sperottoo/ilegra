//
//  Character.swift
//
//  Created by Fabrício Sperotto Sffair
//  Copyright © 2018 Fabrício Sperotto Sffair. All rights reserved.
//
//  Modelo do Character, onde o JSON originalmente vem em uma estutura:
//  -Root
//      -Results
//          -Character
//              -Thumbnail
//  Foi utilizado o Decodable nativo para o parsing.
//

import Foundation
import UIKit

struct Root : Decodable {
    let data : Results
}

struct Results : Decodable {
    let results : [Character]
}

class Character : Decodable {
    
    struct Participations : Decodable {
        let available : Int
    }
    
    let id : Int
    let thumbnail : Thumbnail
    let name : String
    let description : String
    let comics : Participations
    let events : Participations
    let series : Participations
    let stories : Participations
    
    
    enum CharacterCodingKeys :  String, CodingKey {
        case id
        case name
        case description
        case thumbnail
        case comics
        case events
        case series
        case stories
        
    }
    
    required init(from decoder: Decoder) throws {
        let char = try decoder.container(keyedBy: CharacterCodingKeys.self)
        name = try char.decode(String.self, forKey: .name)
        id = try char.decode(Int.self, forKey: .id)
        thumbnail = try char.decode(Thumbnail.self, forKey: .thumbnail)
        description = try char.decode(String.self, forKey: .description)
        comics = try char.decode(Participations.self, forKey: .comics)
        events = try char.decode(Participations.self, forKey: .events)
        series = try char.decode(Participations.self, forKey: .series)
        stories = try char.decode(Participations.self, forKey: .stories)
        
    }
    
    static func == (lhs: Character, rhs: Character) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name && lhs.thumbnail == rhs.thumbnail
    }
    
    
    /// Método onde busca a lista de personagens e faz o Parsing para retornar em um bloco
    /// a lista já em sua classe.
    ///
    /// - Parameters:
    ///   - offset: Caso ja tenha algo em tela exibido, deve ser passado a partir de qual índice
    ///     deve-se realizar a busca no WS
    ///   - limit: Limite de resultados de retorno
    ///   - completionHandler: função recebida para realizar a atribuição, visto que chamadas
    ///     são assíncronas.
    static func GETCharacterList(offset: Int, limit: Int, completionHandler: @escaping ([Character]?, Error?) -> Void) {
        API.GET(offset: offset, limit: limit, endPoint: EndPoints.characterList) { (data, response, error) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            if let responseData = data {
                let decoder = JSONDecoder()
                do {
                    let rootJson = try decoder.decode(Root.self, from: responseData)
                    completionHandler(rootJson.data.results, nil)
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                    completionHandler(nil, error)
                }
            } else if let erro = error {
                completionHandler(nil, erro)
            }
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
    }
    
}


