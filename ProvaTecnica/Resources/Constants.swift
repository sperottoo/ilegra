//
//  Constants.swift
//
//
//  Created by Fabrício Sperotto Sffair
//  Copyright © 2018 Fabrício Sperotto Sffair. All rights reserved.
//
//  File das constantes utilizadas pelo projeto

import Foundation

struct APISettings {
    
    static let url = "https://gateway.marvel.com:443/v1/public"
    static let privateKey = "bdb9b923970eeebcb53c256926f18e0f184ed24f"
    static let publicKey  = "432563d92445e36a798291cd3178b184"
}

struct EndPoints {
    static let characterList = "/characters"
}

