//
//  CharacterDetailTableViewCell.swift
//
//  Created by Fabrício Sperotto Sffair.
//  Copyright © 2018 Fabrício Sperotto Sffair. All rights reserved.
//
//  Célula de descrição do detalhe do personagem

import UIKit

class CharacterDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    /// Configuração da celula de detalhe de personagem
    ///
    /// - Parameters:
    ///   - key: String de exibição a ser exibido na parte esquerda da tela
    ///   - value: String de valor a ser exibido na parte direita da tela
    func configureCell(key: String, value: String) {
        
        self.keyLabel?.text = key
        self.valueLabel?.text = value == "" ? "Não há informações" : value
    }

}
