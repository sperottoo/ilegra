//
//  CharactersTableViewCell.swift
//
//  Created by Fabrício Sperotto Sffair
//  Copyright © 2018 Fabrício Sperotto Sffair. All rights reserved.
//
//  Célula da lista de personagens, onde possui de fundo uma imagem e o nome do personagem
//

import UIKit

class CharactersTableViewCell: UITableViewCell {

    @IBOutlet weak var characterView: UIImageView!
    @IBOutlet weak var characterLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    /// Método de configuração de célula.
    ///
    /// - Parameters:
    ///   - url: url da imagem a ser exibido
    ///   - name: nome do personagem a ser exibido
    func configureCell(url: URL?, name: String) {
        if let urlImage = url {
            let placeholder = #imageLiteral(resourceName: "marvelTitleLogo")
            self.activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            self.characterView.kf.setImage(with: urlImage, placeholder: placeholder, options: [.transition(.fade(0.4))], progressBlock: nil, completionHandler: { (img, error, cachetype, url) in
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
            })
        }
        self.characterLabel?.text = name
        
    }

}
