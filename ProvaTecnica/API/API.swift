//
//  API.swift
//
//  Created by Fabrício Sperotto Sffair
//  Copyright © 2018 Fabrício Sperotto Sffair. All rights reserved.
//
//  API para comunicação com o servidor.

import Foundation
import UIKit

struct API {
    
    
    /// Método para realizar uma requisição do tipo Get onde ele pega por padrão a url que está na APISettings.url.
    ///
    /// - Parameters:
    ///   - offset: número inteiro para realizar a requisição com o índice inicial a partir deste número
    ///   - limit: número inteiro máximo de retorno da chamada
    ///   - endPoint: endpoint da chamada.
    ///   - completionHandler: funcão de retorno, visto que é uma chamada assíncrona.
    static func GET(offset: Int = 0, limit: Int = 0, endPoint: String, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        guard let urlWithParams = createUrlWithParam(offset: offset, limit: limit, endPoint: endPoint) else {
            debugPrint("Url inválida")
            return
        }
        self.createRequest(url: urlWithParams, completionHandler: completionHandler)
    }
    
    static func createRequest(url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        
        
        let urlRequest = URLRequest(url: url)
        let session = URLSession.shared
        
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            guard let responseData = data, error == nil else {
                print("Error: did not receive data")
                completionHandler(nil, response, error)
                return
            }
            
            completionHandler(responseData, response, nil)
        }
        task.resume()
    }
    
    static func createUrlWithParam(offset: Int, limit: Int, endPoint: String) -> URL? {
        
        let timeStamp = Int(Date().timeIntervalSince1970 * 1000)
        let hash = "\(timeStamp)\(APISettings.privateKey)\(APISettings.publicKey)"
        
        return URL(string: APISettings.url + endPoint + "?offset=\(offset)&limit=\(limit)&apikey=\(APISettings.publicKey)&hash=\(hash.MD5)&ts=\(timeStamp)")
    }
    
    
}





