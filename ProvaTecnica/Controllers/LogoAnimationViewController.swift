//
//  LogoAnimationViewController.swift
//
//  Created by Fabrício Sperotto Sffair
//  Copyright © 2018 Fabrício Sperotto Sffair. All rights reserved.
//
//  View Controller inicial com uma animação de entrada, onde
//  faz comunicação do WS para buscar alguns personagens inicialmente
//  para a tela inicial de lista não inicializar vazia.
//

import Foundation
import UIKit

class LogoAnimationViewController : UIViewController {
    
    //MARK: - Vars and Structs
    @IBOutlet weak var logoImage : UIImageView!
    
    /// Variáveis de controle para animação e busca no WS
    /// utilizadas para não exibir a tela de personagens antes de
    /// terminar a busca no WS ou antes de finalizar a animação
    private var animationCompleted: Bool = false
    private var objectsFinishedFetching: Bool = false
    private var characters : [Character] = [Character]()
    
    private struct Segues {
        static let CharacterList = "CharacterListSegue"
    }
    
    //MARK : - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.beginAnimation()
        self.fetchDataFromWS()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let navController = segue.destination as? UINavigationController, let charListViewController = navController.childViewControllers.first as? CharacterListTableViewController {
            charListViewController.characters = self.characters
        }
    }
    
    // MARK: - WS Method
    func fetchDataFromWS() {
        Character.GETCharacterList(offset: characters.count, limit: 16, completionHandler: { (results, error) in
            // Sinaliza que ja realizou a chamada do WS
            self.objectsFinishedFetching = true
            
            if let chars = results {
                self.characters = chars
            } else if let erro = error {
                self.completeAnimation()
                debugPrint("\(erro)")
            }
            
            if self.animationCompleted {
                self.completeAnimation()
            }
        })
    }
    
    // MARK: - Animation
    /// Método para começar a animação
    func beginAnimation() {
        self.logoImage.alpha = 0
        DispatchQueue.main.async {
            UIView.animate(
                withDuration: 0.5,
                delay: 0,
                options: UIViewAnimationOptions.curveEaseOut,
                animations: {
                    self.logoImage.alpha = 1
            }) {
                completed in
                if completed {
                    if self.objectsFinishedFetching {
                        self.completeAnimation()
                    } else {
                        self.animationCompleted = true
                    }
                }
            }
        }
    }
    
    /// Método para terminar a animação e, ao terminar, realiza
    /// a troca de tela para a de listagem de personagem.
    func completeAnimation(){
        DispatchQueue.main.async {
            UIView.animate(
                withDuration: 0.5,
                delay: 0,
                options: UIViewAnimationOptions.curveEaseIn,
                animations: {
                    self.logoImage.alpha = 0
            }){
                completed in
                self.performSegue(withIdentifier: Segues.CharacterList, sender: nil)
            }
        }
    }
}
