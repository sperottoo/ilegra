//
//  CharacterListTableViewController.swift
//
//  Created by Fabrício Sperotto Sffair
//  Copyright © 2018 Fabrício Sperotto Sffair. All rights reserved.
//
//  View Controller que exibe a lista de personagens.
//

import UIKit
import Kingfisher

class CharacterListTableViewController: LoadMoreTableViewController {

    //MARK: - Variables and structs
    var characters : [Character]?
    private struct Segues {
        static let CharDetail = "CharacterDetailSegue"
    }
    
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Seta a imagem do logo da Marvel na navigationBar
        let imageView = UIImageView(image: #imageLiteral(resourceName: "marvelTitleLogo"))
        imageView.clipsToBounds = true
        navigationItem.titleView = imageView
        
        //Adiciona refresh control
        addRefreshControl()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Prepare for segue para enviar as infos do personagem
        // a ser exibido os detalhes
        if segue.identifier == Segues.CharDetail {
            if let detailedCharacterViewController = segue.destination as? CharacterDetailsTableViewController, let char = sender as? Character {
                detailedCharacterViewController.characater = char
            }
        }
    }

    //MARK: - Table View DataSource
    /// Configurta a celula, passando a url da imagem do persnagem
    /// e o nome a ser exibido
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterCell", for: indexPath) as? CharactersTableViewCell {
            
            guard let chars = characters else { return UITableViewCell() }
            let char = chars[indexPath.row]
            let url = URL(string: char.thumbnail.path + "." + char.thumbnail.ext)
         
            cell.configureCell(url: url, name: char.name)
            
            return cell
        }

        return UITableViewCell()
        
    }
    
    
    /// Define um tamanho de tela para a exibição
    /// legível do personagem e sua imagem.
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
        
    }
    
    /// Define a ação de selecionar a row do personagem
    /// para exibir os detalhes.
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let char = characters?[indexPath.row] else { return }
        self.performSegue(withIdentifier: Segues.CharDetail, sender: char)
        
    }
    
    
    /// Adiciona pull to refresh
    /// para atualizar a tela caso necessário.
    private func addRefreshControl() {
        let rc = UIRefreshControl()
        rc.removeTarget(self, action: nil, for: .allEvents)
        rc.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        rc.tintColor = UIColor.white
        tableView.refreshControl = rc
    }
    
    
    /// Refresh com clear do refresh control.
    @IBAction func refresh(_ sender: UIRefreshControl) {
        loadMore(true, with: sender)
    }
    

}


/// Informaões necessárias para a LoadMoreViewController
/// onde é necessário os records (itens a exibir)
/// o limit para chamada de WS, onde está atualmente setado para 16
/// um titulo para quando não achar resultados
/// e o méthodo para buscar dados do WS
extension CharacterListTableViewController {
    override var records: [Any] {
        get {
            return self.characters ?? [Character]()
        }
        set {
            if let characters = newValue as? [Character] {
                self.characters = characters
            }
        }
    }
    
    override var limit: Int {
        return 16
    }
    
    override var noResultsTitle: String {
        return "Nenhum personagem encontrado"
        
    }

    override func fetchDataFromWS(withOffset offset: Int, andLimit limit: Int, completionBlock completion: @escaping ([Any]?, Error?) -> Void) {
        Character.GETCharacterList(offset: offset, limit: limit) {
            (chars, error) in
            completion(chars, error)
        }
    }
}

