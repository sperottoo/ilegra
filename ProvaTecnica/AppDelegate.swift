//
//  AppDelegate.swift
//  ProvaTecnica
//
//  Created by Fabrício Sperotto Sffair on 2018-01-09.
//  Copyright © 2018 Fabrício Sperotto Sffair. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }

}

